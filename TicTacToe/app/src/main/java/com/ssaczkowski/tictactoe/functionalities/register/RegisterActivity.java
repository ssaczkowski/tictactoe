package com.ssaczkowski.tictactoe.functionalities.register;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.ssaczkowski.tictactoe.R;
import com.ssaczkowski.tictactoe.functionalities.findGame.FindGameActivity;

import model.User;

public class RegisterActivity extends AppCompatActivity {

    private EditText etName, etEmail, etPassword;
    private Button btnRegister;
    private FirebaseAuth fireBaseAuth;
    private FirebaseFirestore database;
    private ProgressBar progressBarregister;
    private ScrollView scrollFormRegister;

    public RegisterActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        etName = findViewById(R.id.edit_text_name);
        etEmail = findViewById(R.id.edit_text_email);
        etPassword = findViewById(R.id.edit_text_password);

        btnRegister = findViewById(R.id.button_register);
        progressBarregister = findViewById(R.id.progress_bar_register);
        scrollFormRegister = findViewById(R.id.form_register);

        fireBaseAuth = FirebaseAuth.getInstance();
        database = FirebaseFirestore.getInstance();

        events();

    }

    private void events() {

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String name = etName.getText().toString();
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();

                if (name.isEmpty()) {
                    etName.setError(getString(R.string.error_name_register));
                    etName.requestFocus();
                } else if (email.isEmpty()) {
                    etEmail.setError(getString(R.string.error_email_register));
                    etEmail.requestFocus();
                } else if (password.isEmpty()) {
                    etPassword.setError(getString(R.string.error_password_register));
                    etPassword.requestFocus();
                } else {
                    createUser(name,email,password);
                }

            }
        });


    }

    private void createUser(final String name, String email, String password) {
        changeFormRegisterVisibility(false);

        fireBaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    final FirebaseUser user = fireBaseAuth.getCurrentUser();

                    User userObject = new User(name,0,0);

                    database.collection("users").document(user.getUid()).set(userObject).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            updateUI(user);
                        }
                    });
                }else{
                    Toast.makeText(RegisterActivity.this,getString(R.string.error_on_register),Toast.LENGTH_LONG);
                    updateUI(null);
                }
            }
        });

    }

    private void updateUI(FirebaseUser user) {
        if(user != null){
            finish();
            Intent findGameIntent = new Intent(RegisterActivity.this, FindGameActivity.class);
            startActivity(findGameIntent);
        }else{
            changeFormRegisterVisibility(true);
            etPassword.setError(getString(R.string.error_on_data_register));
            etPassword.requestFocus();
        }
    }

    private void changeFormRegisterVisibility(Boolean showForm) {
        progressBarregister.setVisibility(showForm ? View.GONE : View.VISIBLE);
        scrollFormRegister.setVisibility(showForm ? View.VISIBLE : View.GONE);
    }


}
