package com.ssaczkowski.tictactoe.functionalities.findGame;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.ssaczkowski.tictactoe.R;

public class FindGameActivity extends AppCompatActivity {

    private TextView tvLoadingMessage;
    private ProgressBar progressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_game);


        tvLoadingMessage = findViewById(R.id.text_view_loading);
        progressBar = findViewById(R.id.progress_bar_findgame);

        progressBar.setIndeterminate(true);
        tvLoadingMessage.setText(getString(R.string.charging));
    }

    @Override
    protected void onPause() {
        super.onPause();

        FirebaseAuth.getInstance().signOut();
    }
}
