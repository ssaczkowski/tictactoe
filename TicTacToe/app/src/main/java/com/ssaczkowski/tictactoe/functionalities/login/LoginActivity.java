package com.ssaczkowski.tictactoe.functionalities.login;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.ssaczkowski.tictactoe.R;
import com.ssaczkowski.tictactoe.functionalities.findGame.FindGameActivity;
import com.ssaczkowski.tictactoe.functionalities.register.RegisterActivity;

public class LoginActivity extends AppCompatActivity {


    private EditText etEmail,etPassword;
    private Button btnLogin;
    private ScrollView scrollFormLogin;
    private ProgressBar progressBarLogin;
    private Button btnRegister;
    private FirebaseAuth firebaseAuth;
    private Boolean tryLogin = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etEmail = findViewById(R.id.edit_text_email);
        etPassword = findViewById(R.id.edit_text_password);
        btnLogin = findViewById(R.id.button_login);
        scrollFormLogin = findViewById(R.id.form_login);
        progressBarLogin = findViewById(R.id.progress_bar_login);
        btnRegister = findViewById(R.id.button_register);

        firebaseAuth = FirebaseAuth.getInstance();

        events();

    }

    private void events() {

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();

                if (email.isEmpty()) {
                    etEmail.setError(getString(R.string.error_email_register));
                    etEmail.requestFocus();
                } else if (password.isEmpty()) {
                    etPassword.setError(getString(R.string.error_password_register));
                    etPassword.requestFocus();
                } else {
                    changeFormLoginVisibility(false);
                    loginUser(email,password);
                }

            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent registerActivityIntent = new Intent(LoginActivity.this, RegisterActivity.class);

                startActivity(registerActivityIntent);

            }
        });

    }

    private void loginUser(String email, String password) {
        firebaseAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                tryLogin = true;
                if(task.isSuccessful()){
                    FirebaseUser user = firebaseAuth.getCurrentUser();
                    updateUI(user);
                }else{
                    Log.w("ERROR","signInError: ", task.getException());
                    Toast.makeText(LoginActivity.this,getString(R.string.error_on_login),Toast.LENGTH_LONG);
                    updateUI(null);
                }
            }
        });
    }


    private void updateUI(FirebaseUser user) {
        if(user != null){
            Intent findGameIntent = new Intent(LoginActivity.this, FindGameActivity.class);
            startActivity(findGameIntent);
        }else{
            changeFormLoginVisibility(true);
            if(tryLogin) {
                etPassword.setError(getString(R.string.error_on_data_login));
                etPassword.requestFocus();
            }
        }
    }

    private void changeFormLoginVisibility(Boolean showForm) {
        progressBarLogin.setVisibility(showForm ? View.GONE : View.VISIBLE);
        scrollFormLogin.setVisibility(showForm ? View.VISIBLE : View.GONE);
    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseUser currentUser = firebaseAuth.getCurrentUser();
        updateUI(currentUser);

    }
}
