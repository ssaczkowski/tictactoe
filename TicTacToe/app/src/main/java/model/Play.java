package model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Play {

    private String playerOneId;
    private String playerTwoId;
    private List<Integer>  selectedCells;
    private boolean playersTurnOne;
    private String winnerId;
    private Date created;
    private String abandonmentId;

    public Play() {
    }

    public Play(String playerOneId) {
        this.playerOneId = playerOneId;
        this.playerTwoId = "";
        initCells();
        this.playersTurnOne = true;
        this.created = new Date();
        this.winnerId = "";
        this.abandonmentId = "";
    }

    private void initCells() {
        this.selectedCells = new ArrayList<>();
        for (int i = 0 ; i < 9 ; i++){
            this.selectedCells.add(new Integer(0));
        }
    }

    public String getPlayerOneId() {
        return playerOneId;
    }

    public void setPlayerOneId(String playerOneId) {
        this.playerOneId = playerOneId;
    }

    public String getPlayerTwoId() {
        return playerTwoId;
    }

    public void setPlayerTwoId(String playerTwoId) {
        this.playerTwoId = playerTwoId;
    }

    public List<Integer> getSelectedCells() {
        return selectedCells;
    }

    public void setSelectedCells(List<Integer> selectedCells) {
        this.selectedCells = selectedCells;
    }

    public boolean isPlayersTurnOne() {
        return playersTurnOne;
    }

    public void setPlayersTurnOne(boolean playersTurnOne) {
        this.playersTurnOne = playersTurnOne;
    }

    public String getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(String winnerId) {
        this.winnerId = winnerId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getAbandonmentId() {
        return abandonmentId;
    }

    public void setAbandonmentId(String abandonmentId) {
        this.abandonmentId = abandonmentId;
    }
}
