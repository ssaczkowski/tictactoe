package model;


public class User {

    private String name;
    private Integer points;
    private Integer gamesPlayed;

    public User() {
    }

    public User(String name, Integer points, Integer gamesPlayed) {
        this.name = name;
        this.points = points;
        this.gamesPlayed = gamesPlayed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    public Integer getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(Integer gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }
}
